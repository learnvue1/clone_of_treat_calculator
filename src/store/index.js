import Vue from "vue";
import Vuex from "vuex"

Vue.use(Vuex)

const store = new Vuex.Store({
    state:{ 
        tip:0,
        bill: 0,
        tax: 0,
        total_amount: 0,
        gov_tax: 16
    },
    getters:{
        get_tip:(state) => {return state.tip},
        get_tax:(state) => {return state.tax},
        get_total_amount:(state) => { return state.total_amount},
        get_bill:(state) => {return state.bill}

    },
    mutations: {
        add_tip:(state,amount) => { state.tip = amount},
        add_bill:(state,amount) => { state.bill = amount},
        calculate_tax:(state) => {state.tax = (state.bill * (state.gov_tax/100))},
        calculate_amount:(state) => {
            let total_amount = state.tip + state.bill + state.tax
            state.total_amount = total_amount
        }
    },
    actions:{
        add_bill(state,amount)
        {
            this.commit("add_bill",parseFloat(amount))
            this.commit("calculate_tax")
            this.commit("calculate_amount")

        },
        add_tip(state,amount)
        {
            this.commit("add_tip",parseFloat(amount))
            this.commit("calculate_amount")

        }

    }

});

export default store;